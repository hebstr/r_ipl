source("scripts/_setup.R")

tab_2.gts <-
list(uv =
       tbl_uvregression(d.1[with(.ti, c(y$surv$vars, uv))],
                        method = coxph,
                        y = do.call(Surv, list(!!!syms(.ti$y$surv$vars))),
                        exponentiate = .m$exponentiate,
                        pvalue_fun = .s$pvalue$format,
                        show_single_row = -all_cat_non_dicho(d.1, .ti$uv),
                        hide_n = TRUE),
     mv = 
       tbl_regression(.m$mv,
                      exponentiate = .m$exponentiate,
                      pvalue_fun = .s$pvalue$format,
                      show_single_row = all_dichotomous())) |> 
  map(~ . |>
        tab_format(ci = list(label = .s$ci$label,
                             data = .s$ci$data),
                   estim_sep_int = .s$sep$int,
                   model_mv = .m$mv,
                   ref_no = .s$labs$no,
                   label_header = .t$header$label,
                   vargrp_levels = .t$vargrp$levels,
                   bold_p = .s$pvalue$seuil) |> 
        rename_lab(var = sympt_type,
                   lab = "{level} symptoms")) |>  
  tbl_merge(tab_spanner = .t$spanner)

#tab_data(tab_2.gts)

tab_2 <-
tab_2.gts |> 
  gt_format(title = eval(.t$title$tab_2),
            note = c(eval(.t$note$strata),
                     .m$note_mv,
                     .t$note$pvalue),
            note_p_ajust = .t$note$p_ajust,
            acro = list(data = opts$acro,
                        sep_ext = .s$sep$ext),
            width = .t$width$tab_2,
            theme = !!.t$theme) |> 
  inject()

#tab_data(tab_2)

tab_2 |> 
  output(width = .t$width$tab_2,
         suffix = .s$fdr$output_suffix,
         print = .m$mv)
