#' @export
acro <- \(..., sep) {

e <- env("~" = \(x, y) glue("{enexpr(x)}{sep}{y}"))

list(...) |> 
  map(~ eval(enexpr(.), e)) %>%
  set_names(str_extract(., "\\w+"))

}


#' @export
str_acro <- \(..., collapse) {

paste0(str_c(c(...), collapse = collapse), ".")

}


#' @export
str_label <- \(data, ...) {

c(...) |> 
  map_chr(~ with(data, eval(sym(.))) |> var_label()) |> 
  str_flatten_comma(" and ")

}


#' @export
str_fig <- \(title,
             note = "",
             acro = "",
             sub_size = 7.5) {

glue("{title}<br><span style='font-size:{sub_size}pt'>{note} {acro}</span>")
  
}


#' @export
str_cap <- \(fct, str) {

cap <- str_sub(str, end = 1)
call <- do.call(fct, list(cap))
str_replace_all(str, cap, call)

}


#' @export
str_u <- \(...) {

str_c(unlist(c(...)), collapse = "|")

}


#' @export
assign2 <- \(..., data) {

assign(paste0(...), data, envir = .GlobalEnv)

}


#' @export
all_cat_non_dicho <- \(data, ...) {

level <- map_int(c(...), ~ with(data, eval(sym(.))) |> nlevels())

c(...)[level > 2]

}


#' @export
merge_estimate_ci <- \(x,
                       name = estimate_ci,
                       ci_data,
                       multi = 1) {

x |> 
  mutate(across(c(estimate, contains("conf")),
                ~ format(round(. * multi, 2), nsmall = 2)),
         {{ name }} := glue("{estimate} ", ci_data))

}


#' @export
rename_lab <- \(x, var, lab) {

var <- enexpr(var)

if (!is_null(grep(var, x$table_body$variable))) {

x <-
x |>
  modify_table_body(~ . |>
    mutate(label = ifelse(variable == var,
                          glue("{glue(lab)} — ref: {reference_level}"),
                          label)))

}

}


#' @export
tab_data <- \(x) {

class <- class(x)[1]
y <- substitute(x)

if (class != "gt_tbl") {

.data <-
assign2(y, "_meta",
        data = x$meta_data |> unnest(cols = "df_stats", names_repair = "unique")) |> 
  expr()
  
.meta_data <-
list(assign2(y, "_meta", data = x$meta_data),
     assign2(y, "_data", data = x$inputs$data %>% filter(.[[2]] != 0))) |> 
  expr()
  
.body_style <-
list(assign2(y, "_body", data = x$table_body),
     assign2(y, "_style", data = x$table_styling$header)) |> 
  expr()

class |> 
  switch("tbl_summary" = eval(.data),
         "tbl_uvregression" = eval(.meta_data),
         "tbl_regression" = eval(.body_style),
         "tbl_merge" = eval(.body_style),
         "tbl_strata" = eval(.body_style))

} else { 
  
assign2(y, ".gt_style", data = x$`_options`)
  
}

}


#' @export
output <- \(x,
            dir = "output",
            width = NA,
            height = NULL,
            size = NULL,
            suffix = NULL,
            print = NULL,
            ...) {

if (!dir.exists(dir)) {
  
  dir.create(path = dir)
  
  cli_alert_success("dossier {.emph /{dir}} crée dans {.emph {getwd()}}")
  
}
  
if (!is_phantomjs_installed()) install_phantomjs()

y <- enexpr(x)

if (!is_null(suffix)) y <- glue("{y}", suffix)

if (class(x)[1] == "gt_tbl" & is_null(x$`_heading`$title)) y <- glue("{y}_slide")

path <- glue("{dir}/{y}")
.html <- glue("{path}.html")
.svg <- glue("{path}.svg")
.png <- glue("{path}.png")

if (str_detect(class(x)[1], "tbl")) {

    if (class(x)[1] != "gt_tbl") x <- as_gt(x)

    else if (names(x$`_data`)[1] == "variable") {

             if (!str_detect(x$`_data`$var_label, "ref:")[1]) {
    
             print(x$`_data` |> 
                     select(matches("(variable|var_type|test_name)($|_1)")) |> 
                     distinct())
            
             } else if (!is_null(print)) print(print)
      
         }

    gtsave(x, file = .html)
    
    browseURL(.html)
    
    webshot(.html,
            file = .png,
            vwidth = width + width*0.0625,
            vheight = 1,
            zoom = 3)

} else if (is.ggplot(x)) {

       capturePlot(x, .svg, svg,
                   height = size[1],
                   width = size[2],
                   ...) |> 
         browseURL()

       ggsave(.png,
              height = size[1],
              width = size[2],
              ...)
           
           if (!is_null(print)) print(print)

       }

cli_alert_success("enregistré dans {.emph {getwd()}/{path}.png}")

}


#' @export
opts_set <- \(suivi_an = NULL,
              fdr = NULL,
              labs,
              sep, sep_space,
              ci, pvalue,
              font, palette) {

if (!is_null(suivi_an)) {

    if (!suivi_an %in% 1:5) {
      
    abort("suivi_an doit être un entier compris entre 1 et 5")
      
    }

suivi_jr <- suivi_an * 365

} else suivi_jr <- NULL

sep <- map(sep, ~ paste(., ""))

if (sep_space) sep <- map(sep, ~ paste("", .))

switch(ci$lim,
       "[" = { ci_low <- "[" ; ci_high <- "]" },
       "(" = { ci_low <- "(" ; ci_high <- ")" })

ci <-
list(label = paste0(ci_low, ci$label, ci_high),
     data = paste0(ci_low, "{conf.low}", sep$.ci, " {conf.high}", ci_high))

if (is_false(fdr$include)) fdr$output_suffix <- ""

assign("opts",
       list(set = c(suivi_an = suivi_an,
                    suivi_jr = suivi_jr,
                    fdr = list(fdr),
                    pvalue = list(pvalue),
                    labs = list(labs),
                    sep = list(sep),
                    ci = list(ci),
                    font = list(font),
                    palette = list(palette))),
       envir = .GlobalEnv)

}


#' @export
opts_tab <- \(input = list(descr, uv, mv,
                           suppl = NULL),
              vargrp = list(base = NULL,
                            suppl = list(vars = NULL,
                                         before = NULL)),
              suppl_include = FALSE,
              note, title,
              header = NULL,
              spanner = NULL,
              width = NULL,
              theme = NULL) {

add_vctr <- \(to, var, before) {

x <- setdiff(c(to, var), var)

append(x, var, after = match(before, x) - 1)

}

group <- \(...) {

fun <- \(x) map(list(...), ~ .[x]) |> unlist()
  
list(labels = fun(1),
     levels = fun(-1))

}

.vars <- expr(str_c(vars, collapse = ', '))

input$y <-
lst(lm = input$y$lm,
    glm = input$y$glm,
    surv = lst(vars = input$y$surv,
               obj =
                 if (!is_null(vars)) glue("Surv({eval(.vars)})")
                 else NULL))

input$strata <-
lst(vars = input$strata,
    obj =
      if (!is_null(vars)) glue("strata({eval(.vars)})")
      else NULL)

opts_tab <-
lst(input = lst(descr = input$descr,
                uv = input$uv,
                mv = input$mv,
                y = input$y,
                strata = input$strata),
    vargrp = group(vargrp$base),
    note = note,
    title = title,
    header = header,
    spanner = spanner,
    width = width,
    theme = theme)

if (suppl_include) {

opts_tab$input$descr <-
add_vctr(to = input$descr,
         var = c(input$suppl, vargrp$suppl$vars),
         before = vargrp$suppl$before)

opts_tab$input$uv <-
add_vctr(to = input$uv,
         var = c(input$suppl, vargrp$suppl$vars),
         before = vargrp$suppl$before)

opts_tab$vargrp <- group(vargrp$base, vargrp$suppl$vars)

}

assign("opts_tab", opts_tab, envir = .GlobalEnv)

}


#' @export
theme_gt <- \(x,
              width,
              alpha = "arial",
              digit = "arial",
              base = "black",
              color = "lightgrey",
              ...) {
  
x |>
  opt_align_table_header(align = "left") |> 
  opt_table_font(font = alpha) |> 
  tab_options(table.width = px(width),
              table.font.size = px(12),
              table.font.color = base,
              table.background.color = color,
              heading.background.color = "white",
              heading.title.font.size = pct(95),
              heading.border.bottom.style = "none",
              table.border.top.style = "none",
              table.border.bottom.style = "none",
              column_labels.border.top.style = "none",
              column_labels.border.bottom.width = px(1),
              column_labels.border.bottom.color = base,
              column_labels.background.color = "white",
              table_body.border.top.width = px(1),
              table_body.border.top.color = base,
              table_body.border.bottom.width = px(1),
              table_body.border.bottom.color = base,
              table.border.bottom.width = px(1),
              table.border.bottom.color = base,
              table_body.hlines.style = "none",
              container.padding.x = px(10),
              heading.padding = px(10),
              data_row.padding = px(3),
              data_row.padding.horizontal = px(5),
              row.striping.include_table_body = TRUE,
              row.striping.background_color = "white",
              footnotes.marks = "standard",
              footnotes.background.color = "white",
              footnotes.padding = px(1),
              footnotes.font.size = pct(80),
              ...) |> 
  tab_style(style = cell_text(align = "justify"),
            locations = list(cells_title(), cells_footnotes())) |> 
  tab_style(style = cell_text(size = px(11)),
            locations = cells_body(columns = grep("p.v", names(x[[1]])))) |> 
  tab_style(style = cell_text(font = digit),
            locations = cells_body(columns =
                                     map(c("stat", "p.v", "estim"),
                                         ~ grep(., names(x[[1]]))) |> 
                                     unlist()))

}


#' @export
theme_cuminc <- \(title_font = "arial",
                  title_size = 9,
                  title_margin = margin(0, 0, 0, 0),
                  ...) {

theme_classic() %+replace%
  theme(line = element_line(linewidth = 0.3),
        text = element_text(family = title_font),
        axis.title = element_text(face = "bold",
                                  size = 9),
        axis.title.x = element_text(vjust = -1),
        axis.title.y.left = element_text(vjust = 1),
        axis.text = element_text(size = 8),
        legend.position = "none",
        panel.background = element_blank(),
        plot.background = element_blank(),
        plot.margin = margin(0, 0, 0, 0),
        plot.caption = element_textbox(size = title_size,
                                       width = unit(1, "npc"),
                                       margin = title_margin),
        plot.caption.position = "plot",
        ...)

}


#' @export
theme_risktable <- \(font = "arial",
                     table_margin = margin(0, 0, 0, 0),
                     ...) {

list(theme_risktable_default(),
     theme(text = element_text(family = font),
           plot.title = element_text(size = 7.5,
                                     face = "bold",
                                     margin = margin(0, 0, 0, 0)),
           plot.title.position = "plot",
           plot.margin = table_margin,
           panel.background = element_blank(),
           plot.background = element_blank(),
           axis.text.y = element_text(size = 7),
           ...))

}


#' @export
gt_format <- \(x,
               title,
               acro = list(data, sep_ext),
               note = "",
               note_p_ajust = "",
               vargrp = list(label = "", note = ""),
               width,
               theme = NULL,
               slide = FALSE,
               ...) {
  
style <- x$table_styling$header$label

body <-
str_extract(names(x$table_body), ".*label") |>
  na.omit() |> 
  map(~ x$table_body[[.]]) |>
  unlist()

data_acro <-
str_extract(c(style, body, names(x)),
            glue("\\b",
                 str_c(names(acro$data), collapse = "\\b|\\b"),
                 "\\b")) |>
  na.omit() |>
  unique()

if(!is_null(theme)) {

x <-
as_gt(x) |>
  theme_gt(width = width,
           alpha = theme$alpha,
           digit = theme$digit,
           base = theme$base,
           color = theme$color)
  
} else x <- as_gt(x) |> theme_gt(width = width)

if (!slide) {

x <- x |> tab_header(md(title))

    if (sum(grep("coef", names(x[[1]]))) >= 1) {

    x |> 
      tab_footnote(c(str_c(note),
                     str_acro(.estim$base,
                              .estim$ajust,
                              with(acro$data, mget(data_acro[data_acro != "N"])),
                              collapse = acro$sep_ext))) |> 
      tab_footnote(note_p_ajust,
                   cells_column_labels(p.value_2))

    } else {

    x |> 
      tab_footnote(c(str_c(note),
                     str_acro(with(acro$data, mget(data_acro)),
                              collapse = acro$sep_ext))) |> 
      tab_footnote(vargrp$note,
                   cells_body(columns = label,
                              rows = variable %in% vargrp$label))
    }
    
} else x

}


#' @export
tab_format <- \(x,
                ci,
                estim_sep_int,
                model_mv,
                ref_no,
                label_header = "",
                label_stat = "",
                vargrp_levels = "",
                bold_p = "",
                hide_n = TRUE) {

if (class(x)[1] == "tbl_merge") check_by <- x$tbls[[1]]$by
else check_by <- x$by

if (str_detect(class(x)[1], "reg")) {

x <-
x |> 
  modify_table_body(~ . |> 
    mutate(coefficients_label =
             case_when(coefficients_label == "exp(Beta)" ~ "OR",
                       .default = coefficients_label),
           estim_acro =
             case_when(class(x)[1] == "tbl_regression" ~ glue("a{coefficients_label}"),
                       .default = coefficients_label),
           estim_label =
             coefficients_label |>
               case_match("OR" ~ "odds ratio",
                          "HR" ~ "hazard ratio",
                          .default = "regression coefficient"))) %>%
  modify_header(estimate ~ glue("**{unique(.$table_body$estim_acro)} {ci$label}**")) |> 
  modify_table_styling(columns = estimate,
                       rows = !is.na(ci),
                       cols_merge_pattern = paste("{estimate}", ci$data)) |> 
  modify_table_styling(columns = ci,
                       hide = TRUE)

estim <-
list(acro = "{unique(x$table_body$coefficients_label)}",
     label = "{unique(x$table_body$estim_label)}")

assign(".estim",
       list(base = glue(estim$acro, estim_sep_int, estim$label),
            ajust = glue("a", estim$acro, estim_sep_int, "adjusted ", estim$label)),
       envir = .GlobalEnv)

    if (!is_null(model_list_terms_levels(model_mv))) {

    levels <-
    model_list_terms_levels(model_mv) |> 
      select(variable, reference_level) |> 
      distinct()

    x <-
    x |> 
      modify_table_body(~ . |> 
        left_join(levels, by = "variable") |> 
        mutate(level = str_extract(term, glue("(?<={str_u(x$table_body$variable)}).+")),
               label = case_when(var_type == "dichotomous" & !reference_level %in% c(ref_no, NA)
                                 ~ glue("{level} — ref: {reference_level}"),
                                 var_type == "dichotomous" & reference_level %in% c(ref_no, NA)
                                 ~ glue("{label} — ref: {ref_no}"),
                                 .default = label)))

    }

    if (!hide_n) {

        if (length(unique(x$table_body$N_event)) == 1) {

        x <-
        x |> 
          modify_table_body(~ . |> 
            mutate(n_event =
                     case_when(n_event == N_event & var_type == "continuous" ~ NA,
                               .default = n_event))) %>%
          modify_header(n_event ~ glue("**n (N={unique(.$table_body$N_event)})**"))
            
        } else {

        x <-
        x |> 
          modify_table_body(~ . |> 
            mutate(n_event =
                     case_when(n_event == N_event ~ NA,
                               .default = n_event),
                   N_event_uv =
                     case_when(var_type == "categorical" & header_row == FALSE ~ NA,
                               .default = N_event))) |> 
          modify_header(n_event ~ "**n**",
                        N_event_uv ~ "**N**") |> 
          modify_table_body(~ . |> relocate(N_event_uv, .after = n_event))
            
        }

    }

x <-
x |>
  modify_header(label ~ label_header,
                p.value ~ "**p**") |>
  bold_p(t = bold_p) |>
  modify_footnote(everything() ~ NA,
                  abbreviation = TRUE)

} else if (!is_null(check_by)) {
  
x <-
x |>
  add_overall() |>
  modify_table_body(~ . |>
    mutate(across(contains("stat_"), ~ ifelse(str_starts(., "0.0\\d+"), "—", .)))) |>
  modify_header(stat_0 ~ "**Total<br>(N={N})**",
                x$df_by$by_col ~ "**{level}<br>(n={n}, {style_percent(p, digits = 1)}%)**") |>
  modify_spanning_header(x$df_by$by_col ~ glue("**{var_label(x$inputs$data[[check_by]])}**")) |>
  modify_footnote(everything() ~ NA)

} else x <- modify_footnote(x, everything() ~ NA)

if (!is_null(check_by)) {

x <-
x |>
  modify_header(label ~ label_header,
                p.value ~ "**p**") |>
  bold_p(t = bold_p)

} else if (!str_detect(class(x)[1], "reg")) {
  
x <-
x |>
  modify_header(label ~ label_header,
                stat_0 ~ label_stat)

}

x <-
x |>
  modify_table_body(~ . |>
    mutate(row_type = ifelse(variable %in% vargrp_levels, "level", row_type))) |>
  modify_table_styling(columns = label,
                       rows = row_type == "level",
                       text_format = "indent2")

}
