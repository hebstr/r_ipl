source("scripts/_setup.R")

tab_1.gts <-
d.1[.ti$descr] |> 
  tbl_summary(by = sexe,
              statistic = list(all_continuous() ~ "{mean}±{sd}",
                               all_categorical() ~ "{n} ({p})"),
              digits = list(all_continuous() ~ 1,
                            all_categorical() ~ c(0, 1)),
              missing = "no") |> 
  add_p(test = list(all_continuous() ~ "t.test",
                    all_categorical() ~ "chisq.test.no.correct"),
        test.args = all_tests("t.test") ~ list(var.equal = TRUE),
        pvalue_fun = .s$pvalue$format) |> 
  add_stat_label(label = all_continuous() ~ "mean±SD") |> 
  tab_format(label_header = .t$header$label,
             vargrp_levels = .t$vargrp$levels,
             bold_p = .s$pvalue$seuil)

#tab_data(tab_1.gts)

tab_1 <-
tab_1.gts |> 
  gt_format(title = eval(.t$title$tab_1),
            note = .t$note$pvalue,
            acro = list(data = opts$acro,
                        sep_ext = .s$sep$ext),
            vargrp = list(label = .t$vargrp$labels,
                          note = .t$note$vargrp),
            width = .t$width$tab_1,
            theme = !!.t$theme) |> 
  inject()

#tab_data(tab_1)

tab_1 |> 
  output(width = .t$width$tab_1,
         suffix = .s$fdr$output_suffix)
